package fake_security

import (
	"gitlab.com/amedvedev_eyeconweb/http-security"
	"net/http"
)

type fakeSecurity struct {
}

func (f fakeSecurity) PolicyManager() http_security.Manager {
	//TODO implement me
	panic("implement me")
}

func (f fakeSecurity) Can(r *http.Request, action string) (bool, error) {
	return true, nil
}

func (f fakeSecurity) AuthUser(r *http.Request) (http_security.AuthUser, error) {
	//TODO implement me
	panic("implement me")
}

func NewFakeSecurity() http_security.Security {
	return &fakeSecurity{}
}
