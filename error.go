package http_security

import (
	"errors"
)

var (
	// ErrGroupExist ...
	ErrGroupExist = errors.New("group exist")
	// ErrActionExist ...
	ErrActionExist = errors.New("action exist")
	// ErrGroupToActionExist ...
	ErrGroupToActionExist = errors.New("this group is already in action")
	// ErrGroupToGroupExist ...
	ErrGroupToGroupExist = errors.New("this group is already in the group")
	// ErrGroupToGroupRecursively ...
	ErrGroupToGroupRecursively = errors.New("recursive dependency group-to-group")
	// ErrAuthNotFound ...
	ErrAuthNotFound = errors.New("auth not found")
	// ErrForbidden ...
	ErrForbidden = errors.New("forbidden")
)
