package jwt_security

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
)

type KeyPair struct {
	PublicKey  string
	PrivateKey string
}

type KeyUsecase interface {
	PairGenerate() (*KeyPair, error)
}

//RSA Usecase
type rsaUsecase struct {
}

func NewKeypairUsecase() KeyUsecase {
	return &rsaUsecase{}
}

func (k *rsaUsecase) PairGenerate() (*KeyPair, error) {
	// generate key
	privateKey, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, err
	}
	publicKey := &privateKey.PublicKey

	// dump private key to file
	var privateKeyBytes []byte = x509.MarshalPKCS1PrivateKey(privateKey)
	privateKeyBlock := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: privateKeyBytes,
	}
	keyPEM := pem.EncodeToMemory(privateKeyBlock)

	// dump public key to file
	publicKeyBytes, err := x509.MarshalPKIXPublicKey(publicKey)
	if err != nil {
		return nil, err
	}

	publicKeyBlock := &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: publicKeyBytes,
	}

	pubPEM := pem.EncodeToMemory(publicKeyBlock)

	return &KeyPair{
		PublicKey:  string(pubPEM),
		PrivateKey: string(keyPEM),
	}, nil
}
